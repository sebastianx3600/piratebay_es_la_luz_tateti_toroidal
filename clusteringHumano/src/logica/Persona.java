package logica;

public class Persona {

	String nombre;
	int deporte;
	int musica;
	int espectaculo;
	int ciencia;
	
	int valorMaximo = 5;
	int valorMinimo = 1;
	
	public Persona(String nombre, int interesEnDeportes, int interesEnMusica
			, int interesEnEspectaculo, int interesEnCiencia) {
		verificarNombre(nombre);
		verificarValorDeInteres(interesEnDeportes,"Deporte");
		verificarValorDeInteres(interesEnMusica,"Musica");
		verificarValorDeInteres(interesEnEspectaculo,"Espectaculo");
		verificarValorDeInteres(interesEnCiencia,"Ciencia");
		this.nombre = nombre;
		deporte = interesEnDeportes;
		musica = interesEnMusica;
		espectaculo = interesEnEspectaculo;
		ciencia = interesEnCiencia;
	}
	
	private void verificarNombre(String nombre) {
		if(nombre == null) {
			throw new IllegalArgumentException("El nombre no puede ser null");
		}
		if(nombre.equals("")) {
			throw new IllegalArgumentException("El nombre no puede ser '', el nombre estaba vacio");
		}
	}
	
	private void verificarValorDeInteres (int valor, String queEs) {
		if(valor > valorMaximo || valor < valorMinimo) {
			throw new IllegalArgumentException("El " + queEs + " no es un valor valido: " + valor + ", "
					+ "valor esperado entre "+ valorMinimo + " y " + valorMaximo+ ".");
		}
	}
	
	public int getInteresDeporte ()
	{
		return deporte;
	}
	
	public int getInteresMusica ()
	{
		return musica;
	}
	
	public int getInteresEspectaculo ()
	{
		return espectaculo;
	}
	
	public int getInteresCiencia ()
	{
		return ciencia;
	}
	
	public String getNombre ()
	{
		return nombre;
	}
	
	public boolean equals(Persona p) {
		boolean ret = true;
		ret = ret & nombre.equals(p.getNombre());
		ret = ret & this.getInteresCiencia() == p.getInteresCiencia();
		ret = ret & this.getInteresDeporte() == p.getInteresDeporte();
		ret = ret & this.getInteresEspectaculo() == p.getInteresEspectaculo();
		ret = ret & this.getInteresMusica() == p.getInteresMusica();
		return ret;
	}
	
	public int indiceDeSimilaridad(Persona otraPersona) {
		int ret = 0;
		int aux = 0;
		aux = this.getInteresCiencia() - otraPersona.getInteresCiencia();
		ret = ret + hacerPositivo(aux);
		aux = this.getInteresDeporte() - otraPersona.getInteresDeporte();
		ret = ret + hacerPositivo(aux);
		aux = this.getInteresEspectaculo() - otraPersona.getInteresEspectaculo();
		ret = ret + hacerPositivo(aux);
		aux = this.getInteresMusica() - otraPersona.getInteresMusica();
		ret = ret + hacerPositivo(aux);
		return ret;
	}
	
	private int hacerPositivo(int num) {
		if(num>=0) {
			return num;
		}
		return -num;
	}
	
	public String toString() {
		return this.getNombre() + " " + this.getInteresDeporte() + " " + this.getInteresMusica() + " "
	+ this.getInteresEspectaculo() + " " + getInteresCiencia();
		
	}

}

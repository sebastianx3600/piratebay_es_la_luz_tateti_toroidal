package logica;

import java.util.ArrayList;

import datos.JsonPersona;

public class ClusteringHumano {

	ArrayList<Persona> listaPersonas;
	grafoListaVecinos grafo;
	grafoListaVecinos grafoMin;
	
	String nombreArchivoPorDefault = "listaDePersonas";
	String direccionDelArchivo = "src/datos/";
	String archivo = direccionDelArchivo+nombreArchivoPorDefault;
	
	
	public ClusteringHumano() {
		listaPersonas = new ArrayList<Persona>();
		grafo = new grafoListaVecinos();
		grafoMin = new grafoListaVecinos();
	}
	
	public void AgregarPersonaNueva(String nombre, int deporte, int musica, int espectaculo, int ciencia) {
		Persona p = new Persona(nombre, deporte, musica, espectaculo, ciencia);
		agregarPersona(p);
	}
	
	private void agregarPersona(Persona p) {
		//No permitimos repetidos
		if(!yaEstaAgregadoPersona(p.getNombre())) {
			listaPersonas.add(p);
			grafo.agregarListaVacia();
			int nroDeLaPersona = listaPersonas.size() - 1;
			agregarLasRelaciones(nroDeLaPersona);
			separarEnDosGrupos();
		}
	}
	
	public boolean yaEstaAgregadoPersona(String nombre) {
		for(String per: this.getNombresDePersonas()) {
			if(per.equals(nombre)) {
				return true;
			}
		}
		return false;//Me aseguro que no este el nombre repetido
	}
	
	private void agregarLasRelaciones(int nroDeLaPersona) {
		int indiceVecino = 0;
		int indiceRelacion;
		Persona personaARelacionar = listaPersonas.get(nroDeLaPersona);
		for(indiceVecino = 0; indiceVecino < grafo.getCantidadDeFilas(); indiceVecino++) {
			indiceRelacion = listaPersonas.get(indiceVecino).indiceDeSimilaridad(personaARelacionar);
			grafo.agregarArista(nroDeLaPersona, indiceVecino, indiceRelacion);
		}
	}
	
	public int getCantidadDePersonas() {
		return listaPersonas.size();
	}
	
	public ArrayList<Arista> getRelacionesDe(int nroPersona) {
		return grafo.getFila(nroPersona);
	}
	
	public ArrayList<Persona> getListaDePersonas(){
		return listaPersonas;
	}
	
	public ArrayList<Arista> getRelacionesMinDe(int nroPersona) {
		return grafoMin.getFila(nroPersona);
	}
	
	public String toString() {
		String ret = "";
		for(int indice = 0; indice < listaPersonas.size(); indice++) {
			ret = ret + listaPersonas.get(indice) + ": ";
			for(int vecino = 0; vecino < grafo.getFila(indice).size(); vecino++) {
				ret = ret + " " + grafo.getAristaDeFila(indice, vecino) + " ,";
			}
			ret = ret + "\n";
		}
		return ret;
	}
	
	public ArrayList<ArrayList<Integer>> getComponentesConexas() {
		return grafoMin.getComponentesConexas();
	}
	
	public ArrayList<Integer> getVecinosDeAGM(int nroFila){
		//return grafoMin.obtenerListaDeConexionesDe(nroFila);
		return grafoMin.getListaDeVecinosDe(nroFila);
	}
	public Persona getPersona(int nroFila) {
		return listaPersonas.get(nroFila);
	}
	
	 private void generarAGM() {
		 grafoMin = grafo.obtenerAGM();
	 }
	 
	 private void quitarAristaMayor() {
		 grafoMin.quitarAristaMayor();
	 }
	 
	 public void separarEnDosGrupos() {
		 grafoMin = new grafoListaVecinos();
		 generarAGM();
		 quitarAristaMayor();
	 }
	 
	 public int getPesoEntre(int v1,int v2) {
		 return grafoMin.getValorEntre(v1,v2);
	 }
	 
	//JSON
	 
	 public String getNombreArchivoDefault() {					//Devuelvo el nombre del archivo por defecto que vamos a utilizar
		 return nombreArchivoPorDefault;
	 }
	
	public void guardarListaDePersonas(String nombreArchivo) {	//Guardo el arreglo(ArrayList) de personas al JSON
		JsonPersona json = new JsonPersona();
		json.guardarListaPersonas(listaPersonas,direccionDelArchivo+nombreArchivo);
	}
	
	public void recuperarListaDePersonas(String nombreArchivo) {
		try {
			reiniciarLista();
			JsonPersona json = new JsonPersona();
			listaPersonas = json.obtenerListaPersonas(direccionDelArchivo+nombreArchivo);
			//Reconstruyo el grafo
			grafo = new grafoListaVecinos();
			for(int ind = 0; ind < listaPersonas.size(); ind++) {
				grafo.agregarListaVacia();
			}
			//Le metemos las relaciones
			int indiceVecino = 0;
			int indiceRelacion;
			Persona personaARelacionar;
			for(int ind = 0; ind < listaPersonas.size(); ind++) {
				personaARelacionar = listaPersonas.get(ind);
				for(indiceVecino = ind; indiceVecino < grafo.getCantidadDeFilas(); indiceVecino++) {
					indiceRelacion = listaPersonas.get(indiceVecino).indiceDeSimilaridad(personaARelacionar);
					grafo.agregarArista(ind, indiceVecino, indiceRelacion);
				}
				//agregarLasRelaciones(ind);
			}
			separarEnDosGrupos(); //Generamos la tabla minimal
		}catch(Exception e) {}
		
	}
	
	// * * * * * * FIN DE JSON
	
	public void reiniciarLista() {
		listaPersonas = new ArrayList<Persona>();
		grafo = new grafoListaVecinos();
		grafoMin = new grafoListaVecinos();
	}
	
	public ArrayList<String> getNombresDePersonas(){
		ArrayList<String> ret = new ArrayList<String>();
		for(Persona p: this.listaPersonas) {
			ret.add(p.getNombre());
		}
		return ret;
	}

}

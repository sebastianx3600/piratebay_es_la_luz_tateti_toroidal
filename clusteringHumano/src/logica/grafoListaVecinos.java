package logica;

import java.util.ArrayList;

public class grafoListaVecinos {
	
	// Una Arraylist de tipo Arraylist<Integer> representaria las relaciones entre las personas
	//Siendo el Integer valor del ArrayList la relacion con la otra persona
	
	ArrayList<ArrayList<Arista>> grafo;
	
	public grafoListaVecinos(){
		grafo = new ArrayList<ArrayList<Arista>>();
	}
	
	public void agregarListaVacia() {	//agregarVerticeVacia
		grafo.add(new ArrayList<Arista>());
	}
	
	/*
	 * Agrega una arista a la fila y al vecino (en caso de que fila y vecino no sean el mismo)
	 * La Arista se compone con el numero de la fila/vecino con la que se esta relacionando y el segundo valor es el
	 * el int valor pasado
	 * Permitimos relaciones consigo mismo pero solo se crea una Arista.
	 */
	public void agregarArista(int fila, int vecino, int valor) {
		verificarNumeroFilaValido(fila);
		verificarNumeroFilaValido(vecino);
		Arista nueva = new Arista(vecino,valor);
		grafo.get(fila).add(nueva);
		if(fila != vecino) {
			Arista nueva2 = new Arista(fila,valor);
			grafo.get(vecino).add(nueva2);
		}
	}
	
	public int getCantidadDeFilas() {
		return grafo.size();
	}
	
	public Integer cantidadDeAristasDeUnaFila(int fila) {
		verificarNumeroFilaValido(fila);
		return grafo.get(fila).size();
	}
	
	public Arista getAristaDeFila(int fila, int relacionNro) {
		verificarNumeroFilaValido(fila);
		verificarNumeroDeColumna(fila,relacionNro);
		return grafo.get(fila).get(relacionNro);
	}
	
	public ArrayList<Arista> getFila(int fila){
		verificarNumeroFilaValido(fila);
		return grafo.get(fila);
	}
	
	public boolean sonVecinos(int fila,int supuestoVecino) {
		verificarNumeroFilaValido(fila);
		verificarNumeroFilaValido(supuestoVecino);
		boolean ret = false;
		int indice = 0;
		for(indice = 0; indice < this.cantidadDeAristasDeUnaFila(fila); indice++) {
			ret = ret || this.getAristaDeFila(fila, indice).getNumeroVecino() == supuestoVecino;
		}
		return ret;
	}
	
	public ArrayList<Integer> getListaDeVecinosDe(int fila){
		verificarNumeroFilaValido(fila);
		ArrayList<Integer> ret = new ArrayList<Integer>();
		ret.add(fila);
		
		ArrayList<Arista> laFila = new ArrayList<Arista>();
		laFila = this.getFila(fila);
		for(Arista a: laFila) {
			if(!ret.contains(a.getNumeroVecino())) {
				ret.add(a.getNumeroVecino());
			}
		}
		return ret;
	}
	
	public ArrayList<Integer> fusionarListaDeVecinos(ArrayList<Integer> f1, ArrayList<Integer> f2){
		ArrayList<Integer> ret = new ArrayList<Integer>();
		for(Integer num: f1) {
			ret.add(num);
		}
		for(Integer num2: f2) {
			if(!ret.contains(num2)) {
				ret.add(num2);
			}
		}
		return ret;
	}
	
	public ArrayList<Integer> getListaDeConexionesDe(int fila){
		ArrayList<Integer> ret = new ArrayList<Integer>();
		ret = getListaDeVecinosDe(fila);
		for(int indice = 0; indice < ret.size(); indice++) {
			ret = fusionarListaDeVecinos(ret, getListaDeVecinosDe(ret.get(indice)));
		}
		return ret;
	}
	
	//INDICA SI DOS ELEMENTOS DEL GRAFO ESTAN RELACIONADOS
	public boolean sonConexo(int fila, int supuestoVecino) {
		ArrayList<Integer> listaDeConectados = getListaDeConexionesDe(fila);
		return listaDeConectados.contains(supuestoVecino);
	}
	
	public boolean esConexo() {
		if(this.getCantidadDeFilas()==0) {
			return false;
		}
		ArrayList<Integer> visitados = new ArrayList<Integer>();
		ArrayList<Integer> L = new ArrayList<Integer>();
		visitados.add(0);
		L.add(0);
		
		int cont=1;
		while(L.size() !=0) {
			ArrayList<Arista> vecinos = grafo.get( L.get(0) );
			for(Arista a: vecinos) {
				if(!visitados.contains( a.getNumeroVecino())) {
					visitados.add(a.getNumeroVecino());
					cont++;
					L.add(a.getNumeroVecino());
				}
			}
			L.remove(0);
		}
		return cont == grafo.size();
	}
	
	public void setGrafo(ArrayList<ArrayList<Arista>> g) {
		grafo = g;
	}
	
	public ArrayList<ArrayList<Arista>> getGrafo() {
		return grafo;
	}
	
	public grafoListaVecinos obtenerAGM() {
		ArrayList<ArrayList<Arista>> aux = this.AGM();
		grafoListaVecinos nuevoGrafo = new grafoListaVecinos();
		nuevoGrafo.setGrafo(aux);
		return nuevoGrafo;
	}
	
	public ArrayList<ArrayList<Arista>> AGM() {
		if(this.getCantidadDeFilas()==0) {
			return grafo;
		}
		if(!esConexo()) {
			throw new RuntimeException("No se puede generar un camino minimo porque el grafo no es conexo");
		}
		
		ArrayList<Integer> visitados = new ArrayList<Integer>();
		visitados.add(0);
		grafoListaVecinos g = new grafoListaVecinos();
		int vertice=0;
		Arista min=null;
		for(int i=0; i<grafo.size(); i++) {	//creo un nuevo grafo vacio de aristas
			g.agregarListaVacia();
		}
		while( visitados.size() != grafo.size()) {
			
			for(Integer i: visitados) {
				for(Arista a: grafo.get(i)) {
					if(min==null && !visitados.contains(a.getNumeroVecino())) {
						min=a;
						vertice=i;
					}
					if(!visitados.contains(a.getNumeroVecino()) &&  min.getValor() > a.getValor()){	//busco la arista mas peque�a que no sea un vertice que este en la zona segura 
						min=a;
						vertice=i;	
					}
				}
			}
			visitados.add(min.getNumeroVecino());
			g.agregarArista(vertice, min.getNumeroVecino(), min.getValor());
			min=null;
		}
		return g.grafo;
	}
	
	public void quitarAristaMayor(){
		if(this.getCantidadDeFilas()==0) {
			return;
		}
		Arista aristaMayorA=null;
		int verticeA=0;
		
		Arista aristaMayorB=null;
		int verticeB=0;
		
		for ( int i=0; i<grafo.size(); i++) {
			for(Arista a: grafo.get(i)) {
				if(aristaMayorA==null) {
					verticeA=i;
					aristaMayorA= a;
					
					verticeB=a.getNumeroVecino();
					aristaMayorB= getAristaInversa(a,i);
				}
				if(aristaMayorA.getValor() < a.getValor()) {
					verticeA=i;
					aristaMayorA= a;
					
					verticeB=a.getNumeroVecino();
					aristaMayorB= getAristaInversa(a,i);
				}
			}
		}
		grafo.get(verticeA).remove(aristaMayorA);
		grafo.get(verticeB).remove(aristaMayorB);
		
	}
	
	//devuelve la arista que va de B->A 
	public Arista getAristaInversa(Arista a,int vertice) {
		Arista aux = new Arista(vertice,a.getValor());
		for(int i=0; i< grafo.get(a.getNumeroVecino()).size(); i++) {
			for(Arista ar: grafo.get(a.getNumeroVecino())) {
				
				if(aux.equals(ar)) {
					return ar;
				}
			}
		}
		return null;
	}
	
	public ArrayList<ArrayList<Integer>> getComponentesConexas(){
		if(getCantidadDeFilas() == 0) {
			return new ArrayList<ArrayList<Integer>> ();
		}
		ArrayList<ArrayList<Integer>> compConexas= new ArrayList<ArrayList<Integer>>();
		ArrayList<Integer> visitados = new ArrayList<Integer>();
		ArrayList<Integer> grafoAux = getListaDeConexionesDe(0);
		compConexas.add(grafoAux);
		
		for(int i=0; i<grafoAux.size(); i++) {
			visitados.add(grafoAux.get(i));
		}
		for(int i=0; i<grafo.size(); i++) {
			if(!visitados.contains(i)) {
				ArrayList<Integer> siguiente=getListaDeConexionesDe(i);
				compConexas.add(siguiente);
				for(Integer s: siguiente) {
					visitados.add(s);
				}
				
			}
		}
		return compConexas;
	}
	
	public int getValorEntre(int v1,int v2) {
		verificarNumeroFilaValido(v1);
		verificarNumeroFilaValido(v2);
		int ret = 0;
		if(sonConexo(v1,v2)) {
			ArrayList<Arista> vecinos = this.getFila(v1);
			for(Arista a: vecinos) {
				if(a.getNumeroVecino()==v2) {
					ret = a.getValor();
				}
			}
		}
		return ret;
	}
	
	/*
	 *  Codigo defensivo
	 */
	
	public void verificarNumeroFilaValido(int fila) {
		if(fila < 0 || fila >= getCantidadDeFilas()) {
			throw new IndexOutOfBoundsException("El numero de fila (" + fila + ") no es valido"
					+ ", cantidad de filas = " + getCantidadDeFilas());
		}
	}
	
	public void verificarNumeroDeColumna(int fila, int columna) {
		verificarNumeroFilaValido(fila);
		if(columna < 0 || columna >= cantidadDeAristasDeUnaFila(fila)) {
			throw new IndexOutOfBoundsException("El numero de columna (" + columna + ") no es valido"
					+ ", cantidad de columnas de la fila "+ fila +" = " + cantidadDeAristasDeUnaFila(fila));
		}
	}
	
	public String toString() {
		String ret = "";
		int fila = 0;
		int relacion;
		for(fila = 0;fila < this.getCantidadDeFilas(); fila++) {
			ret = ret + fila+": {";
			for (relacion = 0; relacion < this.cantidadDeAristasDeUnaFila(fila);relacion++) {
				ret = ret + ", " + this.getAristaDeFila(fila, relacion);
			}
			ret = ret + "}\n";
		}
		return ret;
	}

}

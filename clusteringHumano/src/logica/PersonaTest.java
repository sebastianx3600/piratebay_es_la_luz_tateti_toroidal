package logica;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PersonaTest {
	
	Persona p;
	
	@Before
	public void crearPersonaTest() {
		p = new Persona("Jose",1,2,3,4);
	}
	
	@Test
	public void compararDosPersonasIgualesTest() {
		Persona p2 = new Persona("Jose",1,2,3,4);
		assertTrue(p.equals(p2));
	}
	
	@Test
	public void compararDosPersonasNoIgualesTest() {
		Persona p2 = new Persona("Joaquin",3,2,1,5);
		assertFalse(p.equals(p2));
	}
	
	@Test
	public void calculoDelIndiceDeSimilaridadTest() {
		Persona p2 = new Persona("Juan",1,2,3,4);
		Persona p3 = new Persona("Paco",2,3,4,5);
		assertTrue(p.indiceDeSimilaridad(p2) == 0 && p.indiceDeSimilaridad(p3) == 4);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaConValoreSuperiorEnCienciaTest() {
		Persona p = new Persona("Sebas",1,2,3,6);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaConValorNegativoEnCienciaTest() {
		Persona p = new Persona("Sebas",1,2,3,0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaConValorSuperiorEnDeporteTest() {
		Persona p = new Persona("Sebas",0,2,3,5);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaConValorNegativoEnDeporteTest() {
		Persona p = new Persona("Sebas",0,2,3,4);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaConValorSuperiorEnMusicaTest() {
		Persona p = new Persona("Sebas",1,6,3,1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaConValorNegativoEnMusicaTest() {
		Persona p = new Persona("Sebas",1,0,3,1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaConValorSuperiorEspectaculoTest() {
		Persona p = new Persona("Sebas",1,2,6,2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaConValorNegativoEnEspectaculoTest() {
		Persona p = new Persona("Sebas",1,2,0,1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaMalNombre() {
		Persona p = new Persona("",1,2,3,2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void crearPersonaMalNombreNull() {
		Persona p = new Persona(null,1,2,3,2);
	}

}

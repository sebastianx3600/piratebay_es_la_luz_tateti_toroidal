package logica;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class AristaTest {
	
	Arista t;
	
	@Before
	public void inicializacionDeUnaArista() {
		t = new Arista(1,2);
		assertTrue(t.getNumeroVecino() == 1 && t.getValor() == 2);
	}
	
	@Test
	public void compararDosAristaIgualesTest() {
		Arista t2 = new Arista(1,2);
		assertTrue(t.equals(t2));
	}
	
	@Test
	public void compararDosAristasNoIgualesTest() {
		Arista t4 = new Arista(1,3);
		assertFalse(t.equals(t4));
	}
	
}

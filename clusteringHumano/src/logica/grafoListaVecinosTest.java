package logica;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class grafoListaVecinosTest {

	@Test
	public void crearUnGrafo() {
		grafoListaVecinos g = new grafoListaVecinos();
		assertTrue(g.getCantidadDeFilas() == 0);
	}
	
	grafoListaVecinos g;
	@Before
	public void inicializoVariableParaTrabajar() {
		g = new grafoListaVecinos();
		g.agregarListaVacia();
	}
	
	@Test
	public void cantidadDeFilasTest() {
		//YA HAY UNA INGRESADA EN Before
		g.agregarListaVacia();
		g.agregarListaVacia();
		assertTrue(g.getCantidadDeFilas() == 3);
	}
	
	@Test
	public void cantidadesDeAristasTest() {
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarArista(0, 1, 1);
		g.agregarArista(0, 2, 2);
		g.agregarArista(0, 3, 3);
		g.agregarArista(0, 4, 4);
		assertTrue(g.cantidadDeAristasDeUnaFila(0) == 4);	//Le agregue 4 aristas a la fila 0
	}
	
	@Test
	public void sonVecinosTest() {
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarArista(0, 1, 5);
		g.agregarArista(0, 2, 5);
		g.agregarArista(1, 3, 5);
		assertTrue(g.sonVecinos(0, 1) == g.sonVecinos(0, 2) && g.sonVecinos(0, 1) == true);
	}
	
	@Test
	public void noSonVecinosTest() {
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarArista(0, 1, 5);
		g.agregarArista(0, 2, 5);
		g.agregarArista(1, 3, 5);
		assertFalse(g.sonVecinos(0, 3));
	}
	
	@Test
	public void fusionarListaDeVecinosTest() {
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarArista(0, 1, 5);
		g.agregarArista(0, 2, 5);
		g.agregarArista(1, 3, 5);
		ArrayList<Integer> lista = g.fusionarListaDeVecinos(g.getListaDeVecinosDe(0), g.getListaDeVecinosDe(3));
		assertTrue(lista.contains(1) && lista.contains(2) && lista.contains(3));
	}
	//obtenerListaDeConexionesDe
	@Test
	public void pertenecenALaListaDeConexionesDeTest() {
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarArista(0, 1, 5);
		g.agregarArista(0, 2, 5);
		g.agregarArista(1, 3, 5);
		
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarArista(4, 5, 5);
		
		ArrayList<Integer> lista = g.getListaDeConexionesDe(0);
		assertTrue(lista.contains(1) && lista.contains(2) && lista.contains(3));
	}
	
	//obtenerListaDeConexionesDe
	@Test
	public void noPertenecenALaListaDeConexionesDeTest() {
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarArista(0, 1, 5);
		g.agregarArista(0, 2, 5);
		g.agregarArista(1, 3, 5);
		
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarArista(4, 5, 5);
		
		ArrayList<Integer> lista = g.getListaDeConexionesDe(0);
		assertFalse(lista.contains(4) || lista.contains(5));
	}
	
	//sonConexo
	@Test
	public void sonConexoTest() {
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarArista(0, 1, 5);
		g.agregarArista(0, 2, 5);
		g.agregarArista(1, 3, 5);
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarArista(4, 5, 5);
		assertTrue(g.sonConexo(0, 1) && g.sonConexo(0, 2) && g.sonConexo(0, 3) && g.sonConexo(3, 2) && g.sonConexo(2, 3));
	}
	
	//sonConexo
	@Test
	public void noSonConexoTest() {
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarArista(0, 1, 5);
		g.agregarArista(0, 2, 5);
		g.agregarArista(1, 3, 5);
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarArista(4, 5, 5);
		assertFalse(g.sonConexo(0, 4) || g.sonConexo(0, 5));
	}
	
	@Test
	public void esConexoElGrafoTest() {
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarArista(0, 1, 1);
		g.agregarArista(1, 2, 1);
		g.agregarArista(2, 3, 1);
		assertTrue(g.esConexo() == true);
	}
	
	@Test
	public void noEsConexoElGrafoTest() {
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarArista(0, 1, 1);
		g.agregarArista(1, 2, 1);
		g.agregarArista(2, 3, 1);
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarArista(4, 5, 1);
		assertTrue(g.esConexo() == false);
	}
	
	/*
	 * obtenerAGM
	 */
	@Test
	public void obtenerAGMCantidadDeFilasTest() {
		g.agregarListaVacia(); //1
		g.agregarListaVacia();//2
		g.agregarListaVacia();//3
		g.agregarArista(0, 1, 5);
		g.agregarArista(1, 2, 5);
		g.agregarArista(0, 2, 10);
		g.agregarArista(2, 3, 5);
		g.agregarArista(1, 3, 10);
		grafoListaVecinos gMin = g.obtenerAGM();
		assertTrue(gMin.getCantidadDeFilas() == 4);
	}
	
	@Test
	public void obtenerAGMConSoloUnaFilaSinAristaTest() {
		//Solo tiene una fila
		grafoListaVecinos gMin = g.obtenerAGM();
		assertTrue(gMin.getCantidadDeFilas() == 1);
	}
	
	@Test
	public void obtenerAGMConSoloUnaFilaConAristaCantidadTest() {
		//Solo tiene una fila
		g.agregarArista(0, 0, 2);
		g.agregarArista(0, 0, 3);
		g.agregarArista(0, 0, 4);
		grafoListaVecinos gMin = g.obtenerAGM();
		assertTrue(gMin.cantidadDeAristasDeUnaFila(0) == 0);
	}
	
	@Test(expected = RuntimeException.class)
	public void obtenerAGMTConDosFilasSinAristaTest() {
		g.agregarListaVacia();
		grafoListaVecinos gMin = g.obtenerAGM();
	}
	
	@Test
	public void obtenerAGMCantidadDeAristasTest() {
		g.agregarListaVacia(); //1
		g.agregarListaVacia();//2
		g.agregarListaVacia();//3
		g.agregarArista(0, 1, 5);
		g.agregarArista(1, 2, 5);
		g.agregarArista(0, 2, 10);
		g.agregarArista(2, 3, 5);
		g.agregarArista(1, 3, 10);
		grafoListaVecinos gMin = g.obtenerAGM();
		assertTrue(gMin.cantidadDeAristasDeUnaFila(0) == 1 && gMin.cantidadDeAristasDeUnaFila(1) == 2 
				&& gMin.cantidadDeAristasDeUnaFila(2) == 2 && gMin.cantidadDeAristasDeUnaFila(3) == 1);
	}
	
	@Test
	public void obtenerAGMAristaValoresTest() {
		g.agregarListaVacia(); //1
		g.agregarListaVacia();//2
		g.agregarListaVacia();//3
		g.agregarArista(0, 1, 5);
		g.agregarArista(1, 2, 7);
		g.agregarArista(0, 2, 10);
		g.agregarArista(2, 3, 6);
		g.agregarArista(1, 3, 10);
		grafoListaVecinos gMin = g.obtenerAGM();
		assertTrue(gMin.getAristaDeFila(0, 0).getValor() == 5 && gMin.getAristaDeFila(1, 1).getValor() == 7
				&& gMin.getAristaDeFila(2, 0).getValor() ==  7 && gMin.getAristaDeFila(2, 1).getValor() ==  6 
				&& gMin.getAristaDeFila(3, 0).getValor() == 6);
	}
	
	@Test
	public void obtenerAGMAristaNoSonVecinosTest() {
		g.agregarListaVacia(); //1
		g.agregarListaVacia();//2
		g.agregarListaVacia();//3
		g.agregarArista(0, 1, 5);
		g.agregarArista(1, 2, 7);
		g.agregarArista(0, 2, 10);
		g.agregarArista(2, 3, 6);
		g.agregarArista(1, 3, 10);
		grafoListaVecinos gMin = g.obtenerAGM();
		assertFalse(gMin.sonVecinos(0, 2) || gMin.sonVecinos(1, 3));
	}
	
	/*
	 * Fin de obtenerAGM
	 */
	
	@Test
	public void quitarAristaMayorYaNoSonVecinosTest() {
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarListaVacia();
		
		g.agregarArista(0, 1, 2);
		g.agregarArista(1, 2, 1);
		g.agregarArista(2, 3, 5);
		g.agregarArista(3, 0, 10);
		g.quitarAristaMayor();
		assertFalse(g.sonVecinos(0, 3));
	}
	
	@Test
	public void quitarAristaMayorSiguenSiendoVecinosTest() {
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarListaVacia();
		
		g.agregarArista(0, 1, 2);
		g.agregarArista(1, 2, 1);
		g.agregarArista(2, 3, 5);
		g.agregarArista(3, 0, 10);
		g.quitarAristaMayor();
		assertTrue(g.sonVecinos(0, 1) && g.sonVecinos(1, 2) && g.sonVecinos(2, 3));
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void exceptionAgregarAristaParametroMalDeFilaTest() {
		g.agregarArista(1, 0, 1);
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void exceptionAgregarAristaParametroMalDeFilaMenorTest() {
		g.agregarArista(-1, 0, 1);
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void exceptionAgregarAristaParametroMalDeColumnaTest() {
		g.agregarArista(0, 1, 1);
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void exceptionAgregarAristaParametroMalDeColumnaMenorTest() {
		g.agregarArista(0, -1, 1);
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void exceptionobtenerAristaDeFilaParametroMalColumnaTest() {
		g.getAristaDeFila(0, 0);
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void exceptionobtenerAristaDeFilaParametroMalColumnaMenorTest() {
		g.getAristaDeFila(0, -1);
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void exceptionobtenerAristaDeFilaParametroMalFilaTest() {
		g.agregarArista(0, 0, 1);
		g.getAristaDeFila(1, 0);
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void exceptionobtenerAristaDeFilaParametroMalFilaMenorTest() {
		g.agregarArista(0, 0, 1);
		g.getAristaDeFila(-1, 0);
	}
	
	//getAristaInversa
	public void getAristaInversaDeUnaAristaExistenteTest() {
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarArista(0, 1, 10);
		g.agregarArista(0, 2, 5);
		assertTrue(g.getAristaInversa(new Arista(1,10), 0).equals(new Arista(0,10))
				&& g.getAristaInversa(new Arista(2,5), 0).equals(new Arista(0,5))
				);
	}
	
	public void getAristaInversaDeUnaAristaQueNoExisteTest() {
		g.agregarListaVacia();
		g.agregarListaVacia();
		g.agregarArista(0, 1, 10);
		g.agregarArista(0, 2, 5);
		assertTrue(g.getAristaInversa(new Arista(2,10), 1) == null);
	}
	
}

package interfaz;

import java.awt.Graphics;

import logica.Persona;

public class VerticePos {
	private int x,y,fila;
	Persona p;
	
//	recibe las coordenadas
	public VerticePos(int x, int y,Persona p,int fila) {
		this.x=x;
		this.y=y;
		this.p=p;
		this.fila=fila;
	}
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	public Persona getP() {
		return p;
	}
	public void setP(Persona p) {
		this.p = p;
	}
	public int getFila() {
		return this.fila;
	}
	public boolean yaExiste(int x, int y) {
		return this.x==x && this.y==y;
	}
}

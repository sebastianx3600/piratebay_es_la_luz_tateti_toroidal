package datos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import logica.Persona;

public class JsonPersona {

	ArrayList<Persona> listaPersonas;
	
	public JsonPersona() {
		listaPersonas = new ArrayList<Persona>();
	}
	
	public void guardarListaPersonas(ArrayList<Persona> nuevaLista, String archivoDestino) {
		listaPersonas = nuevaLista;
		guardarEnJSON(archivoDestino);
	}
	
	private void guardarEnJSON(String archivoDestino)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(this);
	
		try
		{
			FileWriter writer = new FileWriter(archivoDestino);
			writer.write(json);
			writer.close();
		}catch(Exception e) {}

	}
	
	public  ArrayList<Persona> getListaPersonas(){
		return listaPersonas;
	}
	
	public ArrayList<Persona> obtenerListaPersonas(String archivo) {
		return leerJSON(archivo).getListaPersonas();
	}
	
	private JsonPersona leerJSON(String archivo)
	{
		if(!existeArchivo(archivo)) {
			return new JsonPersona();
		}
		Gson gson = new Gson();
		JsonPersona ret = null;
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(archivo));
			ret = gson.fromJson(br, JsonPersona.class);
		}
		catch (Exception e) {}
		return ret;
	}
	
	public boolean existeArchivo(String nombreArchivo) {
		File archivo = new File(nombreArchivo);
		return archivo.exists();
	}

}

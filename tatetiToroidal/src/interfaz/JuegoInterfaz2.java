package interfaz;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import codigo.Juego;

import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class JuegoInterfaz2 extends JFrame {

	private JPanel contentPane;
	FondoPanel fondo = new FondoPanel();
	Juego mesaJuego;
	//labels
	JLabel mensaje = new JLabel("-");
	JLabel mensajeTurno = new JLabel("");
	JLabel mensajePuntuacion = new JLabel("");
	JLabel labelDeCantTurnos = new JLabel("-");
	JLabel labelImagenJugador1 = new JLabel("-");
	JLabel lblNewLabel = new JLabel("VS");
	JLabel labelImagenJugador2 = new JLabel("-");

	//botones
	ArrayList<JButton> botones = new ArrayList<JButton>();
	JButton botonReinicio = new JButton("Reiniciar");
	JButton botonFin = new JButton("Terminar");
	
	//auxiliar de botones
	int x = 0;
	int y = 0;
	int cont;
	
	//Jmenu
	JMenuBar menuBar;
	JMenu mnNewMenu;
	JMenuItem mntmNewMenuItem;
	JMenuItem mntmNewMenuItem_1;
	
	//path de las imagenes
	String pathCarpetaUbicacion = "..//imagenes//";
	String pathMiniaturaIcon = "miniatura.png";
	String pathImagenBotonSeleccionar = "choose_pehuen.jpg";
	String pathBotonX = "x_pehuen.png";
	String pathBotonO = "o4_pehuen.jpg";
	String pathFormatoDeLasImagenes = ".jpg";
	String pathImagenDeFondo = "fondoHorrible.jpg";
	
	public void cargarImagenes(Juego mesaJuego, JLabel labelImagen1, JLabel labelImagen2)
	{
		String direccion;
		try {
			direccion = pathCarpetaUbicacion+mesaJuego.getJugador1().getNombre()+pathFormatoDeLasImagenes;
			labelImagen1.setIcon(new ImageIcon(getClass().getResource(direccion)));
		}catch(Exception e) {
			labelImagen1.setText(mesaJuego.getJugador1().getNombre());
		}
		try {
			direccion = pathCarpetaUbicacion+mesaJuego.getJugador2().getNombre()+pathFormatoDeLasImagenes;
			labelImagen2.setIcon(new ImageIcon(getClass().getResource(direccion)));
		}catch(Exception e) {
			labelImagen2.setText(mesaJuego.getJugador2().getNombre());
		}
	}
	
	public void setearBotones()
	{
		for(cont = 0; cont < 9; cont++) {
			botones.add(new JButton());
		}
		x = 0;
		y = 0;
		for(cont = 0; cont < botones.size();cont++) {
			final Integer contFinal = cont;
			final Integer fila = x;
			final Integer columna = y;
			botones.get(cont).setText("/");
			botones.get(cont).setEnabled(true);
			try {
				botones.get(cont).setIcon(new ImageIcon((getClass().getResource(pathCarpetaUbicacion+pathImagenBotonSeleccionar))));
			}catch(Exception e) {
				botones.get(cont).setIcon(new ImageIcon());
				botones.get(cont).setText("/");
			}
			botones.get(cont).addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					botones.get(contFinal).setIcon(new ImageIcon());
					if(mesaJuego.getMarcaTurno().equals("X")) {
						try {
							botones.get(contFinal).setIcon(new ImageIcon(getClass().getResource(pathCarpetaUbicacion+pathBotonX)));
						}catch(Exception e) {
							botones.get(contFinal).setText("X");
						}
					}else {
						try {
							botones.get(contFinal).setIcon(new ImageIcon(getClass().getResource(pathCarpetaUbicacion+pathBotonO)));
						}catch(Exception e) {
							botones.get(contFinal).setText("O");
						}
					}
					mesaJuego.jugadorSeleccionaPosicion(fila, columna);
					botones.get(contFinal).setEnabled(false);
					actualizarMensajes();
					if(!mesaJuego.verificarSiHayGanador().equals("")) {
						mensajeTurno.setText("");
						mensajePuntuacion.setText("");
						return;
					}
				}
			});
			botones.get(cont).setBounds(10+(x*114), 33+(y*120), 55, 55);
			contentPane.add(botones.get(cont));
			//sumo x
			x++;
			if(x>2) {	//sumo y
				x = 0;
				y++;
			}
		}
		
		botonReinicio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mesaJuego.reiniciarTablero();
				for(JButton boton: botones) {
					boton.setEnabled(true);
					try {
						boton.setIcon(new ImageIcon(getClass().getResource(pathCarpetaUbicacion+pathImagenBotonSeleccionar)));
					}catch(Exception e) {
						boton.setIcon(new ImageIcon());
						boton.setText("/");
					}
				}
				actualizarMensajes();
			}
		});
		botonReinicio.setBounds(577, 352, 124, 55);
		contentPane.add(botonReinicio);
		botonFin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mesaJuego.escribirPuntosEnEltxt();
				PuntuacionInterfaz mostrarPuntuacionesForm = new PuntuacionInterfaz(mesaJuego);
				mostrarPuntuacionesForm.setVisible(true);
				dispose();
			}
		});
		botonFin.setBounds(448, 358, 108, 43);
		contentPane.add(botonFin);
	}	//FIN DE SETEAR BOTONES
	
	public void setearLabels ()
	{
		mensaje.setHorizontalAlignment(SwingConstants.CENTER);
		mensaje.setFont(new Font("Kozuka Gothic Pro B", Font.PLAIN, 16));
		mensaje.setForeground(Color.WHITE);
		mensaje.setBounds(426, 66, 219, 43);
		contentPane.add(mensaje);
		mensajeTurno.setHorizontalAlignment(SwingConstants.CENTER);
		mensajeTurno.setFont(new Font("Kozuka Gothic Pro B", Font.PLAIN, 16));
		mensajeTurno.setForeground(Color.WHITE);
		mensajeTurno.setBounds(438, 20, 207, 36);
		contentPane.add(mensajeTurno);
		mensajePuntuacion.setHorizontalAlignment(SwingConstants.CENTER);
		mensajePuntuacion.setFont(new Font("Kozuka Gothic Pro B", Font.PLAIN, 16));
		mensajePuntuacion.setForeground(Color.WHITE);
		mensajePuntuacion.setBounds(438, 144, 207, 36);
		contentPane.add(mensajePuntuacion);
		labelDeCantTurnos.setHorizontalAlignment(SwingConstants.CENTER);
		labelDeCantTurnos.setFont(new Font("Kozuka Gothic Pro B", Font.PLAIN, 16));
		labelDeCantTurnos.setForeground(Color.WHITE);
		labelDeCantTurnos.setBounds(438, 216, 219, 36);
		contentPane.add(labelDeCantTurnos);
		actualizarMensajes();
		labelImagenJugador1.setHorizontalAlignment(SwingConstants.CENTER);
		labelImagenJugador1.setForeground(Color.WHITE);
		labelImagenJugador1.setBounds(10, 352, 55, 55);
		contentPane.add(labelImagenJugador1);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setBounds(127, 352, 52, 55);
		contentPane.add(lblNewLabel);
		labelImagenJugador2.setHorizontalAlignment(SwingConstants.CENTER);
		labelImagenJugador2.setForeground(Color.WHITE);
		labelImagenJugador2.setBounds(242, 352, 55, 55);
		contentPane.add(labelImagenJugador2);
		cargarImagenes(mesaJuego,labelImagenJugador1,labelImagenJugador2);
	}
	
	public void setearJMenu ()
	{
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		mnNewMenu = new JMenu("Juego");
		menuBar.add(mnNewMenu);
		mntmNewMenuItem = new JMenuItem("Nueva ronda");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mesaJuego.reiniciarTablero();
				for(JButton boton: botones) {
					boton.setEnabled(true);
					try {
						boton.setIcon(new ImageIcon(getClass().getResource(pathCarpetaUbicacion+pathImagenBotonSeleccionar)));
					}catch(Exception e) {
						boton.setIcon(new ImageIcon());
						boton.setText("/");
					}
				}
				actualizarMensajes();
			}
		});
		mnNewMenu.add(mntmNewMenuItem);
		mntmNewMenuItem_1 = new JMenuItem("Terminar juego");
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mesaJuego.escribirPuntosEnEltxt();
				PuntuacionInterfaz mostrarPuntuacionesForm = new PuntuacionInterfaz(mesaJuego);
				mostrarPuntuacionesForm.setVisible(true);
				dispose();
			}
		});
		mnNewMenu.add(mntmNewMenuItem_1);
	}	//FIN DE JMENU
	
	public void contentPaneLookAndFeeleIconImage ()
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 725, 476);
		//contentPane = new JPanel(); //antes
		contentPane = new FondoPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		try {
			this.setIconImage(new ImageIcon(getClass().getResource(pathCarpetaUbicacion+pathMiniaturaIcon)).getImage());
		}catch(Exception e) {}
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}catch(Exception e) {}
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JuegoInterfaz2 frame = new JuegoInterfaz2("crewmate","mini_pekka");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void actualizarMensajes() {
		mensaje.setText(mesaJuego.mensajeSituacionDelJuego());
		labelDeCantTurnos.setText("Turno n�mero: "+ Integer.valueOf(mesaJuego.getCantTurnos()));
		if(mesaJuego.getMarcaTurno().equals("X")) {
			mensajeTurno.setText("Juegan las "+mesaJuego.getMarcaTurno());
		}else {
			mensajeTurno.setText("Juegan los "+mesaJuego.getMarcaTurno());
		}
		mensajePuntuacion.setText(mesaJuego.mensajePuntuacionDeJugadorTurno());
	}
	
	public JuegoInterfaz2(String nombre1, String nombre2) {
		mesaJuego = new Juego(nombre1,nombre2);
		contentPaneLookAndFeeleIconImage();
		setearBotones();
		setearLabels();
		setearJMenu();
	}
	
	class FondoPanel extends JPanel{
		private Image imagenFondo;
		@Override
		public void paint(Graphics g) {
			try {
				imagenFondo = new ImageIcon(getClass().getResource(pathCarpetaUbicacion+pathImagenDeFondo)).getImage();
				g.drawImage(imagenFondo, 0, 0, getWidth(), getHeight(), this);
				setOpaque(false);
				super.paint(g);
			}catch(Exception e) {}
		}
	}
}

package interfaz;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import codigo.Juego;
import interfaz.JuegoInterfaz2.FondoPanel;

import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;

public class nombresInput extends JFrame {

	private JPanel contentPane;
	private JTextField textNombre1;
	private JTextField textNombre2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					nombresInput frame = new nombresInput();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public nombresInput() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		//contentPane = new JPanel();
		contentPane = new FondoPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		this.setIconImage(new ImageIcon(getClass().getResource("/imagenes/miniatura.png")).getImage());
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}catch (Exception e){
			e.printStackTrace();
		}
		
		textNombre1 = new JTextField();
		textNombre1.setBounds(118, 35, 250, 41);
		contentPane.add(textNombre1);
		textNombre1.setColumns(10);
		
		textNombre2 = new JTextField();
		textNombre2.setBounds(118, 94, 250, 41);
		contentPane.add(textNombre2);
		textNombre2.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Primer jugador");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setBounds(10, 42, 86, 27);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Segundo jugador");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setBounds(10, 100, 98, 28);
		contentPane.add(lblNewLabel_1);
		
		JButton botonNombres = new JButton("Comenzar");
		botonNombres.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(textNombre1.getText().equals("")) {
					JOptionPane.showMessageDialog(textNombre1, "Complete el primer nombre del jugador");
				} else {
					if(textNombre2.getText().equals("")) {
						JOptionPane.showMessageDialog(textNombre2, "Complete el segundo nombre del jugador");
						
					} else {
						if(textNombre2.getText().equals(textNombre1.getText())) {
							JOptionPane.showMessageDialog(textNombre2, "los jugadores deben tener nombres diferentes");
						}else {
						JuegoInterfaz2 otroFrame = new JuegoInterfaz2(textNombre1.getText(),textNombre2.getText());
						otroFrame.setVisible(true);
						dispose();
						}
					}
				}
			}
		});
		botonNombres.setBounds(174, 172, 136, 49);
		contentPane.add(botonNombres);
	}
	
	class FondoPanel extends JPanel{
		private Image imagenFondo;
		@Override
		public void paint(Graphics g) {
			imagenFondo = new ImageIcon(getClass().getResource("/imagenes/fondo_pehuen.jpg")).getImage();
			g.drawImage(imagenFondo, 0, 0, getWidth(), getHeight(), this);
			setOpaque(false);
			super.paint(g);
		}
	}
}

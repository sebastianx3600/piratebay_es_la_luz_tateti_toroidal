package codigo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JLabel;

public class Juego {
	
	String turno;
	
	Jugador j1, j2;
	
	Tablero tablero;
	
	private int cantTurnos;
	
	private int puntosJug1, puntosJug2;

	String ruta = "src/datos/puntuaciones.txt";
	/*	ACERCA DEL TXT puntuaciones.txt
	 * La primera linea indica la cantidad de partidas que se guardaron
	 * Se almacenan los datos de a 5 renglones por juego
	 * La primera linea es del nombre del jugador 1, la segunda linea es para su puntaje, o la cantidad de veces que gano
	 * La tercera linea es del nombre del jugador 2, la cuarta linea es para su puntaje
	 * La quinta es para la cantidad de turnos o movimientos hubo en el juego
	 */
	
	public Juego (String nombreJugador1, String nombreJugador2)
	{
		tablero = new Tablero();
		if(nombreJugador1 == null || nombreJugador2 == null) {
            throw new IllegalArgumentException("El nombre de un jugador no puede ser null");
		}
		j1 = new Jugador(nombreJugador1,"X");
		j2 = new Jugador(nombreJugador2,"O");
		puntosJug1 = 0;
		puntosJug2 = 0;
		
		turno = elegirQuienEmpieza();
		cantTurnos = 1;
	}
	
	public int getCantTurnos()
	{
		return cantTurnos;
	}
	
	public void sumarUnTurno()
	{
		cantTurnos++;
	}
	
	public String getTurno()
	{
		return turno;
	}
	
	public String getMarcaTurno()
	{
		if(j1.getNombre().equals(turno)) {
			return j1.getMarca();
		}
		return j2.getMarca();
	}
	
	public void cambiarTurno()
	{
		if(getTurno().equals(j1.getNombre())) {			//Si anteriormente era el turno de J1 ahora lo es de J2
			turno = j2.getNombre();
		}else {
			turno = j1.getNombre();
		}
	}
	
	// jugadorSeleccionaPosicion recibe un jugador y dos posiciones, uno X y otro Y
	// Devuelve True si se pudo hacer el movimiento, False sino
	public void jugadorSeleccionaPosicion(int posX, int posY)
	{
		if(this.sePuedeSeguirJugando()) {
			Jugador jug;
			if(getTurno().equals(j1.getNombre())) {
				jug = j1;
			} else {
				jug = j2;
			}
			
			if (!posicionOcupada(posX, posY)) {
				tablero.setMovimiento(posX, posY, jug);
				sumarUnTurno();
				if(this.verificarSiHayGanador().equals(jug.getNombre())) {	//ME FIJO SI GANO Y ENTONCES LE SUMO UN PUNTO
					sumarPunto(jug);
				}
				cambiarTurno();
			}
		}
	}
	
	//INFORMA POR MEDIO DE UN String CUAL ES LA SITUACION DEL JUEGO
	public String mensajeSituacionDelJuego()
	{
		if(sePuedeSeguirJugando()) {
			return "Es el turno de " + this.getTurno();
		}
		//No se puede seguir jugando porque alguien gano o hay empate
		if(verificarEmpate()) {
			return "Empate";
		}
		return this.verificarSiHayGanador() + " GAN�!";
	}
	public String mensajePuntuacionDeJugadorTurno() {
		return "Puntuaci�n: "+getPuntuacionTurno(this.getTurno());
	}
	
	public Jugador getJugador1() {
		return j1;
	}
	
	public Jugador getJugador2() {
		return j2;
	}
	
	public void reiniciarTablero()
	{
		tablero = new Tablero();
	}
	
	public String elegirQuienEmpieza() {
		int num = (int) (Math.random() * 10) + 1;
		return num>=5 ? j1.getNombre() : j2.getNombre();
	}
	
	public boolean posicionOcupada(int x, int y)
	{
		return !(this.tablero.getLugar(x, y).equals("/"));
	}
	
	//Le suma un punto al jugador pasado por parametro
	public void sumarPunto(Jugador jugadorGanador)
	{
		if(j1.getNombre().equals(jugadorGanador.getNombre())){
			puntosJug1++;
		}else {
			puntosJug2++;
		}
	}
	
	public int getPuntuacionTurno(String jugadorNombre)
	{
		if(j1.getNombre().equals(jugadorNombre)) {
			return puntosJug1;
		}
		return puntosJug2;
	}
	
	public int getPuntosJugador1() {
		return puntosJug1;
	}
	
	public int getPuntosJugador2() {
		return puntosJug2;
	}
	
	/*
	 * METODOS PARA LA FINALIZACI�N DEL JUEGO
	 */
	//Devuelve el nombre del jugador que gano, si nadie gano, entonces solo devuelve "" (nada, no null)
	public String verificarSiHayGanador ()
	{
		if(esGanador(j1)) {
			return j1.getNombre();			//Jugador1 es el ganador
		}
		
		if(esGanador(j2)) {
			return j2.getNombre();			//Jugador2 es el ganador
		}
		
		return "";							//No hay ganador, aun
	}
	
	// Verifica si hay espacio disponible o si alguien gano
	public boolean sePuedeSeguirJugando ()
	{
		return tablero.hayPosicionSinUsar() && verificarSiHayGanador() == "";
	}
	
	public boolean verificarEmpate()
	{
		if(!sePuedeSeguirJugando()) {	
			//NO SE PEUDE SEGUIR JUGANDO, ALGUIEN GANO O NO HAY MAS ESPACIO (EMPATE)
			if(verificarSiHayGanador().equals("")) {
				return true;
			}
		}
		return false;
	}

	/*
	 * METODOS PARA LA MANIPULACION DEL TXT Y DE PUNTAJES
	 */
	
	private void crearArchivo()
	{
		try {
			File archivo = new File(ruta);
			if(!existeArchivo()) {
				//Debo crear el archivo
				archivo.createNewFile();
			}
		}catch(Exception e) {}
	}
	
	private boolean existeArchivo()
	{
		File archivo = new File(ruta);
		return archivo.exists();
	}
	
	private void escribirLineasAlFinal(ArrayList<String> lineasAgregar)
	{
		try {
			File archivo;
			HashMap<Integer,String> lineas = new HashMap<Integer,String>();
			//PRIMERO LEO
			archivo = new File(ruta);
			FileReader fr = new FileReader (archivo);
			BufferedReader br = new BufferedReader(fr);
			String linea;

			//Se agrega una partida mas
			lineas.put(0, String.valueOf(Integer.parseInt(br.readLine())+1));
			
			int x = 1;
	        while((linea=br.readLine())!=null) {
	        	lineas.put(x, linea);
	        	x++;
	        }
	        
	        //LO VUELVO A ESCRIBIR
			BufferedWriter bw;
			bw = new BufferedWriter(new FileWriter(archivo));
			for(x = 0; x < lineas.size(); x++) {
				bw.write(lineas.get(x).toString() + "\n");
			}
			
			//AGREGO LA LINEA AL FINAL
			for(x = 0; x < lineasAgregar.size(); x++) {
				bw.write(lineasAgregar.get(x).toString() + "\n");
			}
			bw.close();
		}catch(Exception e) {}
	}
	
	public void escribirPuntosEnEltxt()
	{
		if(!existeArchivo()) {
			this.crearArchivo();
		}
		ArrayList<String> lineas = new ArrayList<String>();
		lineas.add(getJugador1().getNombre());
		lineas.add(String.valueOf(getPuntosJugador1()));
		lineas.add(getJugador2().getNombre());
		lineas.add(String.valueOf(getPuntosJugador2()));
		lineas.add(String.valueOf(getCantTurnos()));
		escribirLineasAlFinal(lineas);
	}
	
	public ArrayList<String> getPartida(int numero)
	{
		ArrayList<String> ret = new ArrayList<String>();
		if(cantidadDePartidas()>=numero) {
			try {
				File archivo;
				archivo = new File(ruta);
				FileReader fr = new FileReader (archivo);
				BufferedReader br = new BufferedReader(fr);
				String linea;
				
				//LA PRIMERA LINEA NO ES LO QUE IMPORTA, LA SALTEO
				linea=br.readLine();
				int cant = 1;
				cant = (numero-1)*5;
				
		        while(cant!=0) {
		        	linea=br.readLine();
		        	cant--;
		        }
		        
		        //AGARRO LOS DATOS DE LA PARTIDA QUE INDICA numero
		        for(cant = 1; cant <= 5;cant++) {
		        	ret.add(br.readLine());
		        }
			}catch(Exception e) {}
		}
		return ret;
	}
	
	public int cantidadDePartidas()
	{
		try {
			if(this.existeArchivo()) {
				File archivo;
				archivo = new File(ruta);
				FileReader fr = new FileReader (archivo);
				BufferedReader br = new BufferedReader(fr);
				String linea = br.readLine();
				return Integer.parseInt(linea);
			}
		}catch(Exception e) {}
		return 0;
	}
	
	/*
	 * FIN DE FUNCIONES PARA EL TXT
	 */
	
	// sumarPosicion es necesario para abarcar las jugadas toroidales
	public int sumarPosicion(int p1, int sumar) {
		int ret = p1 + sumar;
		if(ret > 2) {
			ret = ret - 3;
		}
		if(ret < 0) {
			ret = ret + 3;
		}
		return ret;
	}
	
	//Recibe una marca, y dos enteros que son la fila y columna del tablero, compara si el valor de la celda
	//es igual al de la marca (true), y devuelve false si no lo es
	public boolean sonDelMismoBando(String marca, int p1, int p2)
	{
		return tablero.getLugar(p1,p2).equals(marca);
	}
	
	public boolean esGanador(Jugador jugador)
	{
		boolean ret = false;
		boolean retAux = true;
		int x, y = 0;
		int otroX, otroY = 0;
		//Reviso por fila
		for(x = 0; x<3 ;x++) {
			retAux = true;
			if(tablero.getLugar(x, y).equals(jugador.getMarca())) {
				otroX = x;
				otroY = sumarPosicion(y,1);
				retAux = retAux && sonDelMismoBando(jugador.getMarca(),otroX,otroY);
				otroY = sumarPosicion(otroY,1);
				retAux = retAux && sonDelMismoBando(jugador.getMarca(),otroX,otroY);
				ret = ret || retAux;
			}
		}
		//Reviso por columna
		x = 0;
		for(y = 0; y<3 ; y++) {
			retAux = true;
			if(tablero.getLugar(x, y).equals(jugador.getMarca())) {
				otroX = sumarPosicion(x,1);
				otroY = y;
				retAux = retAux && sonDelMismoBando(jugador.getMarca(),otroX,otroY);
				otroX = sumarPosicion(otroX,1);
				retAux = retAux && sonDelMismoBando(jugador.getMarca(),otroX,otroY);
				ret = ret || retAux;
			}
		}
		//RevisarDiagonal de derecha inferior
		for(x = 0; x<3; x++) {
			for(y = 0; y<3; y++) {
				retAux = true;
				if(tablero.getLugar(x, y).equals(jugador.getMarca())) {
					otroX = sumarPosicion(x,1);
					otroY = sumarPosicion(y,1);
					retAux = retAux && sonDelMismoBando(jugador.getMarca(),otroX,otroY);
					
					otroX = sumarPosicion(x,2);
					otroY = sumarPosicion(y,2);
					retAux = retAux && sonDelMismoBando(jugador.getMarca(),otroX,otroY);
					ret = ret || retAux;
				}
			}
		}
//		revisarDiagonal de izquierda inferior
			for(x = 0; x<3; x++) {
				for(y = 0; y<3; y++) {
					retAux = true;
					if(tablero.getLugar(x, y).equals(jugador.getMarca())) {
						otroX = sumarPosicion(x,1);
						otroY = sumarPosicion(y,-1);
						retAux = retAux && sonDelMismoBando(jugador.getMarca(),otroX,otroY);
						
						otroX = sumarPosicion(x,2);
						otroY = sumarPosicion(y,-2);
						retAux = retAux && sonDelMismoBando(jugador.getMarca(),otroX,otroY);
						ret = ret || retAux;
					}
				}
			}
		return ret;
	}
}

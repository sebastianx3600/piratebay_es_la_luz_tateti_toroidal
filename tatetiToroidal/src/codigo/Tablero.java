package codigo;

import java.util.ArrayList;

public class Tablero {

	private String[][] tablero;
	
	Tablero()
	{
		tablero = new String[3][3];
		for(int x = 0; x<3 ;x++) {
			for(int y = 0; y<3 ; y++) {
				tablero[x][y]="/";
			}
		}
	}
	
	/*
	 * setMovimiento() es cuando un jugador eligio hacer un movimiento
	 */
	public void setMovimiento(int p1, int p2, Jugador jugador)
	{
		tablero[p1][p2] = jugador.getMarca();
	}
	
	/*
	 * LAS SIGUIENTES FUNCIONES SON PARA VERIFICAR SI HAY GANADOR
	 */
	
	public String getLugar(int p1, int p2)
	{
		return tablero[p1][p2];
	}
	
	public boolean hayPosicionSinUsar()
	{
		for(int x = 0; x<3 ;x++) {
			for(int y = 0; y<3 ; y++) {
				if(getLugar(x, y).equals("/")) {
					return true;
				}
			}
		}
		return false;
	}
}
